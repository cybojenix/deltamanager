#!/usr/bin/env python
#
# Copyright (C) 2014  Anthony King
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from __future__ import print_function

from getpass import getpass
import requests

if not hasattr(__builtins__, 'raw_input'):
    raw_input = input

__author__ = "Anthony 'Cybojenix' King"
__program__ = "simple uploader"

base_url = "http://api.teameos.org"
login_url = "%s/v1/auth/login/" % base_url
request_url = "%s/v1/auth/request_upload/" % base_url
upload_url = "%s/v1/auth/upload/" % base_url
logout_url = "%s/v1/auth/logout/" % base_url

username = "eos"
password = None


def _check_message(r):
    try:
        r_json = r.json()
    except ValueError:
        raise Exception('invalid data type')
    if r_json.get('result', 'failed') == 'failed':
        d = r_json.get('data', {})
        m = d.get('message', 'Unknown error')
        raise Exception(m)


def login(un=username, pw=password):
    un = un or raw_input("Please enter your username: ")
    pw = pw or getpass("please enter your password: ")
    p = {'username': un, 'password': pw}
    r = requests.post(login_url, data=p)
    _check_message(r)
    return r.json()['data']['sessionid']


def logout(s):
    p = {'sessionid': s}
    r = requests.post(logout_url, data=p)
    _check_message(r)
    return


def request_upload(dev, name, s):
    dev = dev or raw_input('Please enter the device name: ')
    name = name or raw_input('Please enter the file name: ')
    p = {'device': dev,
         'name': name,
         'sessionid': s}
    r = requests.post(request_url, data=p)
    _check_message(r)
    return r.json()['data']['reference_id']


def upload(ref, f, ses):
    f = f or raw_input('Please enter the file location: ')
    files = {'file': open(f, 'rb')}
    p = {'reference_id': ref,
         'sessionid': ses}
    r = requests.post(upload_url, files=files, data=p)
    _check_message(r)
    return r.json()['data']['file_id']


def cli():
    parser = argparse.ArgumentParser(prog=__program__)
    parser.add_argument('-u', action='store', dest='user',
                        default=username, help='Username for api')
    parser.add_argument('-p', action='store', dest='password',
                        default=password, help='Password for api')
    parser.add_argument('-d', action='store', dest='device',
                        default='', help='Device codename or id')
    parser.add_argument('-n', action='store', dest='name',
                        default='', help='File name')
    parser.add_argument('-f', action='store', dest='loc',
                        default='', help='File location')
    options = parser.parse_args()

    ses = login(options.user, options.password)
    ref = request_upload(options.device, options.name, ses)
    f_id = upload(ref, options.loc, ses)
    logout(ses)
    print("file uploaded successfully. file_id=%s" % str(f_id))


if __name__ == '__main__':
    try:
        import argparse
    except ImportError:
        argparse = None
        raise Exception('You must have `argparse` installed to use '
                        ' the cli version of this script')
    cli()