from __future__ import absolute_import

from os.path import dirname, join, getsize, isdir, isfile, realpath
from os import environ, listdir, remove, makedirs
import sys
from time import sleep

from runabove import Runabove, exception
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

sys.path.append(dirname(dirname(realpath(__file__))))
environ.setdefault("DJANGO_SETTINGS_MODULE", "eos.settings")

import django
django.setup()

from api.v1._auth.errors import RequestExists
from api.v1._auth.utils import create_request
from eos.settings import RUNABOVE, DOWNLOAD_STORAGE
from api.v1.common.errors import FileExists
from api.v1.common.models import Device, Owner
from api.v1.common.utils import make_md5sum, create_file

watch_path = join(dirname(dirname(realpath(__file__))), 'd/files')
if not isdir(watch_path):
    makedirs(watch_path)

############## Runabove stuff ##############


def start_connection():
    return Runabove(RUNABOVE.get('key'),
                    RUNABOVE.get('secret'),
                    RUNABOVE.get('consumer'))


def get_container(r):
    try:
        return r.containers.get_by_name(
            RUNABOVE.get('region'),
            RUNABOVE.get('container')
        )
    except exception.ResourceNotFoundError:
        return None


def get_object(c, fn):
    try:
        return c.get_object_by_name(fn)
    except exception.ResourceNotFoundError:
        return None


def create_object(c, fn, d):
    c.create_object(fn, d)


def delete_object(o):
    o.delete()


def list_objects(c):
    return c.list_objects()


def get_file_path(owner, manu, device, name):
    return RUNABOVE.get('path') % {
        'owner': owner,
        'manu': manu,
        'device': device,
        'name': name
    }

############## Requests stuff ##############


class R_(object):
    def __init__(self, device, owner, name):
        self.device = device
        self.owner = owner
        self.name = name

    def delete(self):
        pass


def get_req(device, owner, name):
    try:
        return create_request(owner.user, device, name)
    except (FileExists, RequestExists):
        return R_(device, owner, name)


def upload_file(f_path, device, owner, name, r):
    c = get_container(r)
    p = get_file_path(owner.user.username,
                      device.manufacturer.code_name,
                      device.code_name,
                      name)
    create_object(c, p, open(f_path, 'rb'))


def process_req(req, p, r):
    if isinstance(req, R_):
        if not process_marked_file(req, r):
            print("%s: file exists already" % p)
            return remove(p)
    n = req.name
    d = req.device
    o = req.owner
    upload_file(p, d, o, n, r)
    print("%s: uploaded." % p)
    md5sum = make_md5sum(p)
    size = getsize(p)
    try:
        create_file(o, d, n, size, md5sum)
    except FileExists:
        pass
    req.delete()
    remove(p)


############## Watchdog stuff ##############


def parse_path(p, w_path=watch_path):
    return p[len(w_path)+1:].split('/')


def handle_event(p):
    try:
        o, m, d, f = parse_path(p)
    except ValueError:
        print('%s: not valid path' % p)
        return remove(p)
    try:
        o = Owner.objects.get(user__username=o)
        d = Device.objects.get(code_name=d)
    except (Device.DoesNotExist, Owner.DoesNotExist):
        print('%s: device/owner does not exist' % p)
        return remove(p)
    req = get_req(d, o, f)
    process_req(req, p, start_connection())


class AwesomeHandler(FileSystemEventHandler):
    def on_created(self, event):
        if isfile(event.src_path):
            handle_event(event.src_path)

    def on_moved(self, event):
        if isfile(event.dest_path):
            handle_event(event.dest_path)


def register_watcher():
    event_handler = AwesomeHandler()
    observer = Observer()
    observer.schedule(event_handler,
                      path=watch_path,
                      recursive=True)
    observer.start()
    return observer


############## init stuff ##############


def process_marked_file(req, r):
    p = get_file_path(req.owner.user.username,
                      req.device.manufacturer.code_name,
                      req.device.code_name,
                      req.name)
    c = get_container(r)
    return not bool(get_object(c, p))


def process_files(device, owner, _dir, r):
    reqs = []
    for f in listdir(_dir):
        reqs.append(get_req(device, owner, f))
    for req in reqs:
        name = req.name
        f_path = join(_dir, name)
        process_req(req, f_path, r)


def handler(owner, device, r):
    manu_name = device.manufacturer.code_name
    dev_name = device.code_name
    own_name = owner.user.username
    _dir = DOWNLOAD_STORAGE % {
        'owner': own_name,
        'manu': manu_name,
        'device': dev_name,
        'name': ''
    }
    if isdir(_dir):
        process_files(device, owner, _dir, r)


def main_init():
    owners = Owner.objects.all()
    devices = Device.objects.all()
    r = start_connection()
    for owner in owners:
        for device in devices:
            handler(owner, device, r)


# I have known watchdog to randomly die
# this will keep it working.
def watch_starter():
    breaker = True
    error_count = 0
    o = register_watcher()
    while o.is_alive() and breaker:
        try:
            sleep(5)
        except KeyboardInterrupt:
            breaker = False
            print("finishing up last processes.")
            print("Hit ctrl-C again to kill current process.")
        except (Exception, BaseException) as e:
            print("Error: %d:" % error_count, e)
            if error_count >= 5:
                breaker = False
                print("Too many errors caught. Killing.")
    if breaker:
        watch_starter()


if __name__ == '__main__':
    print('starting init phase..')
    main_init()
    print('finished init. Starting event watcher')
    watch_starter()