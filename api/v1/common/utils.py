from __future__ import absolute_import

from django.contrib.auth.models import User

from .errors import (UserExists, OwnerExists, ManufacturerExists,
                     DeviceExists, FileExists)
from .models import Owner, Device, File, Manufacturer

from eos.settings import RUNABOVE, DOWNLOAD_STORAGE

from hashlib import md5, sha1
import hmac
from os import path
from time import time

try:
    from urllib import urljoin
except ImportError:
    from urllib.parse import urljoin


def make_md5sum(filename, bs=524288):
    """efficiently get md5 of file

    The `blocksize` should be set to the most optimal value.
    512k gave the best results (1.826s vs 2.513s for 5 runs)
    """
    hasher = md5()
    with open(filename, 'rb') as f:
        buf = f.read(bs)
        while buf:
            hasher.update(buf)
            buf = f.read(bs)
    return hasher.hexdigest()


def create_user(username, password, first_name="", last_name="",
                email="", is_staff=False, is_super=False):

    try:
        User.objects.get(username__exact=username)
    except User.DoesNotExist:
        pass
    else:
        raise UserExists(username)

    user = User.objects.create_user(username,
                                    password=password,
                                    first_name=first_name,
                                    last_name=last_name,
                                    email=email)
    user.is_staff = is_staff
    if is_super:
        user.is_staff = True
        user.is_superuser = True
    user.save()

    return user


def create_owner(user):
    try:
        Owner.objects.get(user=user)
    except Owner.DoesNotExist:
        pass
    else:
        raise OwnerExists(user.username)
    return Owner.objects.create(user=user)


def create_user_owner(username, password, first_name='', last_name='',
                      email='', is_staff=False, is_super=False):
    user = create_user(username, password, first_name, last_name,
                       email, is_staff, is_super)
    owner = create_owner(user)
    return user, owner


def create_manufacturer(code_name, full_name):
    try:
        Manufacturer.objects.get(code_name=code_name)
        Manufacturer.objects.get(full_name=full_name)
    except Manufacturer.DoesNotExist:
        pass
    else:
        raise ManufacturerExists(code_name)
    return Manufacturer.objects.create(code_name=code_name,
                                       full_name=full_name)


def create_device(code_name, full_name, manufacturer):
    try:
        Device.objects.get(code_name=code_name,
                           manufacturer=manufacturer)
    except Device.DoesNotExist:
        pass
    else:
        raise DeviceExists((code_name, manufacturer.code_name))
    return Device.objects.create(code_name=code_name,
                                 full_name=full_name,
                                 manufacturer=manufacturer)


def create_manu_device(manu_code_name, manu_full_name,
                       device_code_name, device_full_name):
    manu = create_manufacturer(manu_code_name, manu_full_name)
    device = create_device(device_code_name, device_full_name, manu)
    return manu, device


def create_file(owner, device, name, size, md5sum,
                version='', old_version=''):
    try:
        File.objects.get(owner=owner,
                         device=device,
                         name=name)
    except File.DoesNotExist:
        pass
    else:
        raise FileExists((owner.user.username, device.code_name, name))
    return File.objects.create(owner=owner,
                               device=device,
                               name=name,
                               version=version,
                               size=size,
                               md5sum=md5sum,
                               old_version=old_version)


def get_runabove_temp_url(file_obj, duration=60):
    method = 'GET'
    expires = int(time() + duration)
    file_path = DOWNLOAD_STORAGE % {
        'name': file_obj.name,
        'owner': file_obj.owner.user.username,
        'device': file_obj.device.code_name,
        'manu': file_obj.device.manufacturer.code_name
    }
    object_path = path.join('/v1/',
                            RUNABOVE.get('account'),
                            RUNABOVE.get('container'),
                            file_path)
    hmac_body = "%s\n%s\n%s" % (method, expires, object_path)
    sig = hmac.new(RUNABOVE.get('temp_url_key'), hmac_body, sha1).hexdigest()
    s = urljoin(RUNABOVE.get('storage'), object_path)
    d = '?temp_url_sig=%s&temp_url_expires=%s' % (sig, expires)
    return urljoin(s, d)