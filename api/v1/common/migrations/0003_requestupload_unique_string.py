# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0002_auto_20140920_1247'),
    ]

    operations = [
        migrations.AddField(
            model_name='requestupload',
            name='unique_string',
            field=models.CharField(default='', unique=True, max_length=150),
            preserve_default=True,
        ),
    ]
