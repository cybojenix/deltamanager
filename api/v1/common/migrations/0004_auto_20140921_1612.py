# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import api.v1.common.validators


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0003_requestupload_unique_string'),
    ]

    operations = [
        migrations.AlterField(
            model_name='device',
            name='code_name',
            field=models.CharField(validators=[api.v1.common.validators.NameValidator()], max_length=25),
        ),
        migrations.AlterField(
            model_name='requestupload',
            name='unique_string',
            field=models.CharField(unique=True, max_length=250, default=''),
        ),
    ]
