# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import api.v1.common.validators


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='device',
            name='code_name',
            field=models.CharField(max_length=10, validators=[api.v1.common.validators.NameValidator()]),
        ),
        migrations.AlterField(
            model_name='device',
            name='full_name',
            field=models.CharField(max_length=25, validators=[api.v1.common.validators.NameValidator()]),
        ),
        migrations.AlterField(
            model_name='file',
            name='name',
            field=models.CharField(max_length=100, validators=[api.v1.common.validators.NameValidator()]),
        ),
        migrations.AlterField(
            model_name='manufacturer',
            name='code_name',
            field=models.CharField(max_length=25, validators=[api.v1.common.validators.NameValidator()]),
        ),
        migrations.AlterField(
            model_name='manufacturer',
            name='full_name',
            field=models.CharField(max_length=25, validators=[api.v1.common.validators.NameValidator()]),
        ),
    ]
