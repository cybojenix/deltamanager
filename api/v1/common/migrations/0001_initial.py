# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings
import api.v1.common.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('code_name', models.CharField(max_length=10)),
                ('full_name', models.CharField(max_length=25)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('version', models.CharField(max_length=20, null=True, blank=True)),
                ('size', models.PositiveIntegerField()),
                ('download_count', models.PositiveIntegerField(default=0)),
                ('md5sum', api.v1.common.fields.Md5SumField()),
                ('old_version', models.CharField(max_length=20, null=True, blank=True)),
                ('date', models.DateTimeField(default=datetime.datetime.now)),
                ('device', models.ForeignKey(to='common.Device', related_name='device')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('code_name', models.CharField(max_length=25)),
                ('full_name', models.CharField(max_length=25)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, related_name='user')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RequestUpload',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('version', models.CharField(max_length=20, null=True, blank=True)),
                ('old_version', models.CharField(max_length=20, null=True, blank=True)),
                ('reference', models.CharField(max_length=32)),
                ('device', models.ForeignKey(to='common.Device')),
                ('owner', models.ForeignKey(to='common.Owner')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='file',
            name='owner',
            field=models.ForeignKey(to='common.Owner', related_name='owner'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='device',
            name='manufacturer',
            field=models.ForeignKey(to='common.Manufacturer', related_name='manufacturer'),
            preserve_default=True,
        ),
    ]
