from __future__ import absolute_import

from django.contrib.auth.models import User
from django.db import models

from .fields import Md5SumField
from .validators import NameValidator
from eos.settings import DOWNLOAD_URL, RUNABOVE

from os.path import join
from datetime import datetime

from hashlib import sha1
import hmac
from time import time
try:
    from urllib import urljoin
except ImportError:
    from urllib.parse import urljoin


class Owner(models.Model):
    user = models.OneToOneField(User, unique=True, related_name='user')

    def can_edit(self, user):
        return self.user == user or user.is_admin

    def total_uploads(self):
        return len(File.objects.filter(owner=self))

    def worked_on_devices(self):
        file_list = File.objects.filter(owner=self)
        return file_list.values_list('device')

    def __str__(self):
        return self.user.username


class Manufacturer(models.Model):
    code_name = models.CharField(max_length=25, validators=[NameValidator()])
    full_name = models.CharField(max_length=25, validators=[NameValidator()])

    def can_edit(self, user):
        return user.is_admin

    def devices(self):
        return Device.objects.filter(manufacturer=self)

    def __str__(self):
        return self.full_name


class Device(models.Model):
    code_name = models.CharField(max_length=25, validators=[NameValidator()])
    full_name = models.CharField(max_length=25, validators=[NameValidator()])
    manufacturer = models.ForeignKey('Manufacturer', related_name='manufacturer')

    def can_edit(self, user):
        return user.is_admin

    def total_uploads(self):
        return len(File.objects.filter(device=self))

    def __str__(self):
        return self.full_name


class File(models.Model):
    owner = models.ForeignKey('Owner', related_name='owner')
    device = models.ForeignKey('Device', related_name='device')
    name = models.CharField(max_length=100, validators=[NameValidator()])
    version = models.CharField(max_length=20, blank=True, null=True)
    size = models.PositiveIntegerField()
    download_count = models.PositiveIntegerField(default=0)
    md5sum = Md5SumField()
    # for Deltas
    old_version = models.CharField(max_length=20, blank=True, null=True)
    date = models.DateTimeField(default=datetime.now)

    def can_edit(self, user):
        return user == self.owner.user or user.is_admin

    def get_public_url(self):
        # adding the name does nothing for the logic
        # it's a quick hack to make wget work properly
        url = join('/v1/files/download/', str(self.id), self.name)
        return url

    def get_direct_url(self):
        dsu = DOWNLOAD_URL if not RUNABOVE else RUNABOVE.get('path') or DOWNLOAD_URL
        file_path = dsu % {
            'name': self.name,
            'owner': self.owner.user.username,
            'device': self.device.code_name,
            'manu': self.device.manufacturer.code_name
        }
        if not RUNABOVE:
            return file_path
        method = 'GET'
        duration = 60
        expires = int(time() + duration)
        if file_path.startswith('/'):
            file_path = file_path[1:]
        object_path = join('/v1/',
                           RUNABOVE.get('account'),
                           RUNABOVE.get('container'),
                           file_path)
        print(object_path)
        hmac_body = "%s\n%s\n%s" % (method, expires, object_path)
        key = RUNABOVE.get('temp_url_key')
        sig = hmac.new(key.encode(),
                       hmac_body.encode(),
                       sha1).hexdigest()
        s = urljoin(RUNABOVE.get('storage'), object_path)
        d = '?temp_url_sig=%s&temp_url_expires=%s' % (sig, expires)
        return urljoin(s, d)

    def __str__(self):
        return self.name


class RequestUpload(models.Model):
    owner = models.ForeignKey('Owner')
    device = models.ForeignKey('Device')
    name = models.CharField(max_length=100)
    version = models.CharField(max_length=20, blank=True, null=True)
    old_version = models.CharField(max_length=20, blank=True, null=True)
    reference = models.CharField(max_length=32)
    unique_string = models.CharField(max_length=250, unique=True, default='')

    def build_u_string(self):
        return "-".join((self.owner.user.username,
                         self.device.code_name,
                         self.name))

    def save(self, *args, **kwargs):
        if not self.unique_string:
            self.unique_string = self.build_u_string()
        super(RequestUpload, self).save(*args, **kwargs)

